package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CreateListOrderPayments {

    private final OrderRetrievalClient orderRetrievalClient;

    public List<OrderPayment> mappedToMessageList() {
        List<Order> payments = makeListPending();
        return payments.stream()
                .map(OrderPayment::generateOrderPayment)
                .collect(Collectors.toList());
    }

    private List<Order> makeListPending() {
        return orderRetrievalClient.getOrderByStatus(Status.PENDING);
    }

}
