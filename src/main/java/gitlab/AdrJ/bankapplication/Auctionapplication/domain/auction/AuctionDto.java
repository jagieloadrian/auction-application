package gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.math.BigDecimal;

@Builder
@Getter
public class AuctionDto {

    @NonNull
    private long ownerId;
    @NonNull
    private long ownerAccountId;
    @NonNull
    private String title;
    @NonNull
    private String description;
    @NonNull
    private int quantity;
    @NonNull
    private BigDecimal price;
    @NonNull
    private int numberOfDays;

}
