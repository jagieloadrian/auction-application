package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order;

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.Auction;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@Setter
@Builder
@Table(name = "Orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "orders_sequence")
    @SequenceGenerator(name = "order_sequence")
    @Setter(AccessLevel.PRIVATE)
    private long id;

    private long ownerId;
    private long ownerAccountId;
    private long auctionId;
    private long clientId;
    private String clientAccountNumber;
    private int quantity;
    private BigDecimal unitPrice;
    @Enumerated(EnumType.STRING)
    private Status status;

    static Order generateOrder(OrderDto orderDto, Auction auction) {
        Order order = new Order();
        order.setAuctionId(auction.getId());
        order.setOwnerId(auction.getOwnerId());
        order.setOwnerAccountId(auction.getOwnerAccountId());
        order.setClientId(orderDto.getClientId());
        order.setClientAccountNumber(orderDto.getClientAccountNumber());
        order.setQuantity(orderDto.getQuantity());
        order.setUnitPrice(auction.getPrice());
        order.setStatus(Status.PENDING);
        return order;
    }

    boolean isStatusPending() {
        if (status.equals(Status.PENDING)) {
            return true;
        }
        return false;
    }

    BigDecimal totalPrice(BigDecimal unitPrice, int quantity) {
        return unitPrice.multiply(new BigDecimal(quantity));
    }

    void changeStatusToPaid() {
        this.status = Status.PAID;
    }
}
