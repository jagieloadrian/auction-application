package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order;

public interface OrderPaymentSenderClient {

    void sendOrderPayment(Order order);
}
