package gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuctionFacade {

    private final CreateAuction createAuction;

    public void create(AuctionDto auctionDto) {
        createAuction.create(auctionDto);
    }
}
