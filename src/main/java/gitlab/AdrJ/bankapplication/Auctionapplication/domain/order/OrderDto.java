package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.math.BigDecimal;

@Builder
@Getter
public class OrderDto {

    private long ownerId;
    private long ownerAccountId;
    private long auctionId;
    private long clientId;
    @NonNull
    private String clientAccountNumber;
    private int quantity;
    private BigDecimal unitPrice;


}
