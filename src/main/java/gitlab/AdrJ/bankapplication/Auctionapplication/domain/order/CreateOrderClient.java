package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order;

public interface CreateOrderClient {
    void create(Order order);
}
