package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CreateOrderPayment {

    public OrderPayment createOrderPayment(Order order) {
        OrderPayment orderPayment = OrderPayment.generateOrderPayment(order);
        return orderPayment;
    }

}
