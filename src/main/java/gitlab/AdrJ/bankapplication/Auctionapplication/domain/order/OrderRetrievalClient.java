package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order;

import java.util.List;

public interface OrderRetrievalClient {

    Order getById(long id);

    List<Order> getAllOrders();

    List<Order> getOrderByStatus(Status status);
}
