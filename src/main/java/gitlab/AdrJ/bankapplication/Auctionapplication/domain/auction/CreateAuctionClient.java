package gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction;

public interface CreateAuctionClient {

    void create(Auction auction);
}
