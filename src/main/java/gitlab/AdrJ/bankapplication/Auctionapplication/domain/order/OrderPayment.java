package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.math.BigDecimal;

@Setter
@Getter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class OrderPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "auction_sequence")
    @SequenceGenerator(name = "auction_sequence")
    @Setter(AccessLevel.PRIVATE)
    private long id;
    private long ownerId;
    private long ownerAccountId;
    @NonNull
    private String clientAccountNumber;
    private BigDecimal totalPrice;
    @NonNull
    private String title;

    static OrderPayment generateOrderPayment(Order order) {
        OrderPayment orderPayment = new OrderPayment();
        orderPayment.setOwnerId(order.getOwnerId());
        orderPayment.setOwnerAccountId(order.getOwnerAccountId());
        orderPayment.setClientAccountNumber(order.getClientAccountNumber());
        orderPayment.setTotalPrice(order.totalPrice(order.getUnitPrice(), order.getQuantity()));
        orderPayment.setTitle(String.format("%d,%d,%d", order.getId(),
                order.getOwnerId(), order.getClientId()));
        return orderPayment;
    }
}
