package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order;

public enum Status {
    PAID, PENDING
}
