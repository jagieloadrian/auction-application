package gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@Setter
@Builder
public class Auction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "auction_sequence")
    @SequenceGenerator(name = "auction_sequence")
    @Setter(AccessLevel.PRIVATE)
    private long id;
    private long ownerId;
    private long ownerAccountId;
    private String title;
    private String description;
    private int quantity;
    private BigDecimal price;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    @Enumerated(EnumType.STRING)
    private ActiveAuction activeAuction;
    private int numberOfDays;

    static Auction createAuction(AuctionDto auctionDto) {
        Auction auction = new Auction();
        auction.setOwnerId(auctionDto.getOwnerId());
        auction.setOwnerAccountId(auctionDto.getOwnerAccountId());
        auction.setTitle(auctionDto.getTitle());
        auction.setDescription(auctionDto.getDescription());
        auction.setQuantity(auctionDto.getQuantity());
        auction.setPrice(auctionDto.getPrice());
        auction.startDate = LocalDateTime.now();
        auction.endDate = auction.getStartDate().plusDays(auctionDto.getNumberOfDays());
        auction.setNumberOfDays(auctionDto.getNumberOfDays());
        auction.setActiveAuction(ActiveAuction.ACTIVE);
        return auction;
    }

    public int decreaseQuantity(int orderQuantity) {
        return quantity = quantity - orderQuantity;
    }

    public boolean isActive() {
        return activeAuction.equals(ActiveAuction.ACTIVE) && quantity > 0;
    }

    public void changeStatusByQuantity() {
        if (quantity <= 0) {
            this.activeAuction = ActiveAuction.INACTIVE;
        }
    }

    public void changeStatusByTime() {
        if (endDate.equals(LocalDateTime.now())) {
            this.activeAuction = ActiveAuction.INACTIVE;
        }
    }

}
