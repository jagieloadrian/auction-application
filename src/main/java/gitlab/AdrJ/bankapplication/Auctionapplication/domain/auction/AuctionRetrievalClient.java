package gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction;

public interface AuctionRetrievalClient {

    Auction getById(long id);
}
