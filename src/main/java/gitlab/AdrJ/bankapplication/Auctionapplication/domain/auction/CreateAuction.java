package gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
class CreateAuction {

    private final CreateAuctionClient createAuctionClient;

    @Transactional
    public void create(AuctionDto auctionDto) {
        Auction auction = Auction.createAuction(auctionDto);
        createAuctionClient.create(auction);
    }
}
