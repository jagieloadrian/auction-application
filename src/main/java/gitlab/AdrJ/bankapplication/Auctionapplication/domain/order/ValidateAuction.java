package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order;

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.Auction;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.AuctionRetrievalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
class ValidateAuction {

    private final AuctionRetrievalClient auctionRetrievalClient;

    void validateQuantity(long auctionId, int quantity) {
        Auction auction = auctionRetrievalClient.getById(auctionId);
        if (auction.getQuantity() < quantity || quantity <= 0) {
            ValidateException.quantityIsNotEnough(quantity);
        }
    }

    void validateEndDate(long auctionId) {
        Auction auction = auctionRetrievalClient.getById(auctionId);
        if (LocalDateTime.now().equals(auction.getEndDate())) {
            ValidateException.auctionIsExpired();
        }
    }

    void validateStatus(long auctionId) {
        Auction auction = auctionRetrievalClient.getById(auctionId);
        if (!auction.isActive()) {
            ValidateException.statusIsNotActive();
        }
    }

    static class ValidateException extends IllegalArgumentException {
        private ValidateException(String message) {
            super(message);
        }

        static void quantityIsNotEnough(int quantity) {
            String message = String.format("Quantity auction is not enough. Should be less than %s",
                    quantity);
            throw new ValidateException(message);
        }

        static void statusIsNotActive() {
            String message = String.format("This auction is not active");
            throw new ValidateException(message);
        }

        static void auctionIsExpired() {
            String message = String.format("This auction is expired");
            throw new ValidateException(message);
        }
    }
}
