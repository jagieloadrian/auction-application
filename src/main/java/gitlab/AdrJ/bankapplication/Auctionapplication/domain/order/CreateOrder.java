package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order;

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.Auction;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.AuctionRetrievalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class CreateOrder {

    private final CreateOrderClient createOrderClient;
    private final ValidateAuction validateAuction;
    private final AuctionRetrievalClient auctionRetrievalClient;
    private final CreateOrderPayment createOrderPayment;
    private final OrderPaymentSenderClient orderPaymentSenderClient;

    public void createOrders(OrderDto orderDto, long auctionId) {
        Auction result = auctionRetrievalClient.getById(auctionId);
        Order order = Order.generateOrder(orderDto, result);
        validateAuction.validateQuantity(auctionId, orderDto.getQuantity());
        validateAuction.validateStatus(auctionId);
        validateAuction.validateEndDate(auctionId);
        createOrderClient.create(order);
        createOrderPayment.createOrderPayment(order);
        result.decreaseQuantity(order.getQuantity());
        result.changeStatusByQuantity();
        result.changeStatusByTime();
        orderPaymentSenderClient.sendOrderPayment(order);
    }
}
