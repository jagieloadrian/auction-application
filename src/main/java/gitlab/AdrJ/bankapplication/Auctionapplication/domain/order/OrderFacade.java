package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderFacade {

    private final CreateOrder createOrder;

    public void create(OrderDto orderDto, long auctionId) {
        createOrder.createOrders(orderDto, auctionId);
    }
}
