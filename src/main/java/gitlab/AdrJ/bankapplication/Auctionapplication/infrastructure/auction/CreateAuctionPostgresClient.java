package gitlab.AdrJ.bankapplication.Auctionapplication.infrastructure.auction;

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.Auction;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.CreateAuctionClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class CreateAuctionPostgresClient implements CreateAuctionClient {

    private final AuctionRepository auctionRepository;

    @Override
    public void create(Auction auction) {
        auctionRepository.save(auction);
    }
}
