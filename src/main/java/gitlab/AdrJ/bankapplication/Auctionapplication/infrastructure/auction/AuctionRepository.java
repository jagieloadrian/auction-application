package gitlab.AdrJ.bankapplication.Auctionapplication.infrastructure.auction;

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.Auction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuctionRepository extends JpaRepository<Auction, Long> {
}
