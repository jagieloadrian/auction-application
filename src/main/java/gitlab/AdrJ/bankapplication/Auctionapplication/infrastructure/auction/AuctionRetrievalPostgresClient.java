package gitlab.AdrJ.bankapplication.Auctionapplication.infrastructure.auction;

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.Auction;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.AuctionRetrievalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class AuctionRetrievalPostgresClient implements AuctionRetrievalClient {

    private final AuctionRepository auctionRepository;

    @Override
    public Auction getById(long id) {
        return auctionRepository.getOne(id);
    }
}
