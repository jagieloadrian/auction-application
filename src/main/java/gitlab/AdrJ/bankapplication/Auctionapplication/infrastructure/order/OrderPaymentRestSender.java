package gitlab.AdrJ.bankapplication.Auctionapplication.infrastructure.order;

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.CreateOrderPayment;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.Order;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.OrderPayment;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.OrderPaymentSenderClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
class OrderPaymentRestSender implements OrderPaymentSenderClient {

    private final CreateOrderPayment createOrderPayment;
    private final RestTemplate restTemplate;
    private final String transferBankAppUrl;

    @Autowired
    public OrderPaymentRestSender(CreateOrderPayment createOrderPayment,
                                  RestTemplate restTemplate,
                                  @Value("${send.payment.bankapp.url}") String transferBankAppUrl) {
        this.createOrderPayment = createOrderPayment;
        this.restTemplate = restTemplate;
        this.transferBankAppUrl = transferBankAppUrl;
    }


    @Override
    public void sendOrderPayment(Order order) {
        HttpEntity<OrderPayment> requestEntity = new HttpEntity<>(createOrderPayment.createOrderPayment(order));
        restTemplate.exchange(transferBankAppUrl,
                HttpMethod.POST,
                requestEntity,
                Void.class);
    }
}
