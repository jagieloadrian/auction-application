package gitlab.AdrJ.bankapplication.Auctionapplication.infrastructure.order;

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.CreateOrderClient;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class CreateOrderPostgresClient implements CreateOrderClient {

    private final OrderRepository orderRepository;

    @Override
    public void create(Order order) {
        orderRepository.save(order);
    }
}
