package gitlab.AdrJ.bankapplication.Auctionapplication.infrastructure.order;

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.Order;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByStatus(Status pending);
}
