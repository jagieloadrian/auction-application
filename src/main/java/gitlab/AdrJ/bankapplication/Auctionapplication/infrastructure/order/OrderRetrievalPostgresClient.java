package gitlab.AdrJ.bankapplication.Auctionapplication.infrastructure.order;

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.Order;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.OrderRetrievalClient;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.Status;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
class OrderRetrievalPostgresClient implements OrderRetrievalClient {

    private final OrderRepository orderRepository;

    @Override
    public Order getById(long id) {
        return orderRepository.getOne(id);
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> getOrderByStatus(Status status) {
        return orderRepository.findByStatus(status);
    }
}
