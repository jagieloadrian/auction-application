package gitlab.AdrJ.bankapplication.Auctionapplication.api.order;

import lombok.Value;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Value
public class OrderRequest {

    @Min(1)
    long clientId;

    @NotNull
    @NotEmpty
    String clientAccountNumber;

    @Min(1)
    int quantity;

}
