package gitlab.AdrJ.bankapplication.Auctionapplication.api.order;

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.CreateListOrderPayments;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.OrderPayment;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
class OrderController {

    private final CreateListOrderPayments createListOrderPayments;

    @GetMapping(path = "/pending")
    public List<OrderPayment> getPendingList() {
        return createListOrderPayments.mappedToMessageList();
    }
}
