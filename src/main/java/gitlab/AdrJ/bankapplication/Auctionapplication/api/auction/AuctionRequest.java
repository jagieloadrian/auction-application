package gitlab.AdrJ.bankapplication.Auctionapplication.api.auction;

import lombok.NonNull;
import lombok.Value;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Value
class AuctionRequest {

    @Min(1)
    long ownerId;
    @Min(1)
    long ownerAccountId;
    @NonNull
    String title;
    @NonNull
    String description;
    @Min(1)
    int quantity;
    @Min(1)
    BigDecimal price;
    @Min(1)
    int numberOfDays;


}
