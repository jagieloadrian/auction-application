package gitlab.AdrJ.bankapplication.Auctionapplication.api.auction;

import gitlab.AdrJ.bankapplication.Auctionapplication.api.order.OrderRequest;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.AuctionDto;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.AuctionFacade;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.OrderDto;
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.OrderFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auctions")
@RequiredArgsConstructor
class AuctionController {
    private final AuctionFacade auctionFacade;
    private final OrderFacade orderFacade;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createAuction(@Valid @RequestBody AuctionRequest auctionRequest) {
        AuctionDto auctionDto = auctionDto(auctionRequest);
        auctionFacade.create(auctionDto);
    }

    @PostMapping(path = "/{auctionId}/orders")
    @ResponseStatus(HttpStatus.CREATED)
    public void createOrders(@PathVariable long auctionId, @Valid @RequestBody OrderRequest orderRequest) {
        OrderDto orderDto = ordersDto(orderRequest, auctionId);
        orderFacade.create(orderDto, auctionId);
    }

    private OrderDto ordersDto(OrderRequest orderRequest, long auctionId) {
        return OrderDto.builder()
                .auctionId(auctionId)
                .clientId(orderRequest.getClientId())
                .clientAccountNumber(orderRequest.getClientAccountNumber())
                .quantity(orderRequest.getQuantity())
                .build();
    }

    private AuctionDto auctionDto(AuctionRequest auctionRequest) {
        return AuctionDto.builder()
                .ownerId(auctionRequest.getOwnerId())
                .ownerAccountId(auctionRequest.getOwnerAccountId())
                .title(auctionRequest.getTitle())
                .description(auctionRequest.getDescription())
                .quantity(auctionRequest.getQuantity())
                .price(auctionRequest.getPrice())
                .numberOfDays(auctionRequest.getNumberOfDays())
                .build();
    }
}
