package gitlab.AdrJ.bankapplication.Auctionapplication

import gitlab.AdrJ.bankapplication.Auctionapplication.api.auction.AuctionController
import gitlab.AdrJ.bankapplication.Auctionapplication.api.order.OrderController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = AuctionApplication.class)
class LoadContextTest extends Specification {

    @Autowired(required = false)
    private AuctionController auctionController;
    @Autowired(required = false)
    private OrderController orderController;

    def "Should create all beans"() {
        expect:
        auctionController
        orderController
    }
}
