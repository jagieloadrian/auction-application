package gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction

import spock.lang.Specification

class AuctionTest extends Specification {
    Auction auction = Auction.builder().quantity(2).activeAuction(ActiveAuction.ACTIVE).build();
    Auction auction2 = Auction.builder().quantity(0).activeAuction(ActiveAuction.ACTIVE).build();


    def "Should have the active status"() {
        given:
        auction
        expect:
        auction.isActive() == true
    }

    def "Should change the status"() {
        when:
        auction2
        then:
        auction2.changeStatusByQuantity()
        auction2.isActive() == false
    }
}
