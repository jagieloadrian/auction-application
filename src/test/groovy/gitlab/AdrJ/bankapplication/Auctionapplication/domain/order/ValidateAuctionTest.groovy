package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.ActiveAuction
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.Auction
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.AuctionRetrievalClient
import spock.lang.Specification

class ValidateAuctionTest extends Specification {
    AuctionRetrievalClient auctionRetrievalClient = Mock(AuctionRetrievalClient.class);
    ValidateAuction validateAuction = new ValidateAuction(auctionRetrievalClient);

    Auction auction = Auction.builder().id(1).activeAuction(ActiveAuction.INACTIVE).quantity(1).build();

    def "Should throw exception when auction is not active"() {
        given:
        auctionRetrievalClient.getById(1) >> auction;
        when:
        validateAuction.validateStatus(1);
        then:
        thrown(ValidateAuction.ValidateException)
    }

    def "Should throw exception when quantity is not enough"() {
        given:
        auctionRetrievalClient.getById(1) >> auction
        when:
        validateAuction.validateQuantity(1, 2);
        then:
        thrown(ValidateAuction.ValidateException)
    }

}
