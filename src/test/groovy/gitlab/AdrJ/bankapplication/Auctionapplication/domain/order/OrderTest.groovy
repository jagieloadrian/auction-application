package gitlab.AdrJ.bankapplication.Auctionapplication.domain.order

import spock.lang.Specification

class OrderTest extends Specification {
    Order order = Order.builder().unitPrice(BigDecimal.valueOf(10)).status(Status.PENDING).build();
    Order order2 = Order.builder().unitPrice(BigDecimal.valueOf(10)).status(Status.PAID).build();


    def "Should multiply price correctly"() {
        given:
        order.totalPrice(order.getUnitPrice(), a)

        expect:
        order.totalPrice(order.getUnitPrice(), a) == b

        where:
        a  | b
        1  | 10
        3  | 30
        24 | 240
    }

    def "Should be a paid status"() {
        given:
        order.changeStatusToPaid();
        expect:
        order.isStatusPending() == false;
    }

    def "Should return false with status paid"() {
        given:
        order2
        expect:
        order2.isStatusPending() == false
    }

}
