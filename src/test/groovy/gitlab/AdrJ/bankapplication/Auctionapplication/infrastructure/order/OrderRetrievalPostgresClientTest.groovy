package gitlab.AdrJ.bankapplication.Auctionapplication.infrastructure.order

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.Order
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.order.OrderRetrievalClient
import spock.lang.Specification


class OrderRetrievalPostgresClientTest extends Specification {

    OrderRepository orderRepository = Mock(OrderRepository);

    OrderRetrievalClient orderRetrievalClient = new OrderRetrievalPostgresClient(orderRepository);

    static Order order = Order.builder().id(1).build();
    static Order order2 = Order.builder().id(2).build();
    static Order order3 = Order.builder().id(3).build();
    static List<Order> orders = new LinkedList<Order>([order, order2, order3]);

    static List<Order> checkOrders = new ArrayList<Order>([order, order2, order3]);

    def "Should return orders list correctly"() {
        given:
        orderRepository.findAll() >> orders
        expect:
        orderRetrievalClient.getAllOrders().containsAll(checkOrders)
    }

    def "Should return order by id correctly"() {
        given:
        orderRepository.getOne(a) >> b;
        expect:
        orderRetrievalClient.getById(a)
        where:
        a | b
        3 | order3
        2 | order2
        1 | order
    }


}
