package gitlab.AdrJ.bankapplication.Auctionapplication.infrastructure.auction

import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.Auction
import gitlab.AdrJ.bankapplication.Auctionapplication.domain.auction.AuctionRetrievalClient
import spock.lang.Specification

class AuctionRetrievalPostgresClientTest extends Specification {

    AuctionRepository auctionRepository = Mock(AuctionRepository);
    AuctionRetrievalClient auctionRetrievalClient = new AuctionRetrievalPostgresClient(auctionRepository);

    static Auction auction = Auction.builder().id(1).build();
    static Auction auction2 = Auction.builder().id(2).build();
    static Auction auction3 = Auction.builder().id(3).build();

    def "Should return auction by id correctly"() {
        given:
        auctionRepository.getOne(a) >> b
        expect:
        auctionRetrievalClient.getById(a) == b
        where:
        a | b
        2 | auction2
        1 | auction
        3 | auction3
    }
}
